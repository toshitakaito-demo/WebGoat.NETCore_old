image: alpine:latest

variables:
  CI_DEBUG_TRACE: "false"
  DOCKER_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA

hello-world:
  stage: chatops
  only: [chat]
  script:
    - echo "Hello World"

stages:
  - chatops
  - build
  - test
  - deploy  # dummy stage to follow the template guidelines
  - review
  - dast
#  - staging
#  - canary
  - production
#  - incremental rollout 10%
#  - incremental rollout 25%
#  - incremental rollout 50%
#  - incremental rollout 100%
  - performance
#  - cleanup

build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker info
  script:
    - docker build -t $DOCKER_IMAGE_TAG .
    - docker push $DOCKER_IMAGE_TAG

test:
  stage: test
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  script:
    dotnet test --no-restore --verbosity normal

review:
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/3.2.4-kube-1.13.12
  stage: review
  variables:
    DEPLOY_HOST: ${CI_ENVIRONMENT_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    APP_NAME: webgoatcore
  script:
    - apk --no-cache add gettext
    - cat WebGoatCore.yml | envsubst | kubectl apply -f -
    - echo http://${CI_ENVIRONMENT_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}/ > environment_url.txt
  environment:
    name: review-${CI_COMMIT_REF_SLUG}
    url: http://${CI_ENVIRONMENT_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}/
  artifacts:
    paths:
      - environment_url.txt
  only:
    - branches

production:
  image: registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image/releases/3.2.4-kube-1.13.12
  stage: production
  variables:
    DEPLOY_HOST: ${CI_ENVIRONMENT_SLUG}.${KUBE_INGRESS_BASE_DOMAIN}
    APP_NAME: webgoatcore
  script:
    - apk --no-cache add gettext
    - cat WebGoatCore.yml | envsubst | kubectl apply -f -
  environment:
    name: production
    url: http://${KUBE_INGRESS_BASE_DOMAIN}/
  only:
    - dev

dast:
  stage: dast
  variables:
    DAST_FULL_SCAN_ENABLED: "false"
    DAST_EXCLUDE_RULES: ”10036,10038”


include:
  - template: Jobs/Build.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Build.gitlab-ci.yml
  - template: Jobs/Test.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Test.gitlab-ci.yml
  - template: Jobs/Code-Quality.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
#  - template: Jobs/Deploy.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml
#  - template: Jobs/Deploy/ECS.gitlab-ci.yml # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy/ECS.gitlab-ci.yml
#  - template: Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/DAST-Default-Branch-Deploy.gitlab-ci.yml
  - template: Jobs/Browser-Performance-Testing.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml
  - template: DAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
